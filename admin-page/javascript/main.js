const BASE_URL = "https://63da521c19fffcd620c63f3e.mockapi.io";
// call api get data
function fetchProductsList() {
  onLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      // console.log(" res:", res.data);
      renderFoodList(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log("err:", err);
    });
}
// first fetch / reload page
fetchProductsList();

// Delete product
function deleteProduct(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      offLoading();
      // fetch data after delete complete
      fetchProductsList();
      // console.log(" res:", res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log("err:", err);
    });
}

// Add product
function addProduct() {
  onLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "POST",
    data: getDataFromForm(),
  })
    .then(function (res) {
      offLoading();
      // fetch data after add complete
      fetchProductsList();
      // console.log(" res:", res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log("err:", err);
    });
}

// Edit
function editProduct(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      // console.log(res.data);
      $("#myModal").modal("show");
      document.getElementById("addSP").disabled = true;
      document.getElementById("IdSP").disabled = true;
      document.getElementById("updateSP").disabled = false;
      displayDataOnForm(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}

// Update
function updateProduct() {
  let data = getDataFromForm();
  onLoading();
  axios({
    url: `${BASE_URL}/products/${data.id}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      offLoading();
      fetchProductsList();
      // console.log(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}
