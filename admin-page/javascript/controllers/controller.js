function onLoading() {
  document.getElementById("loading").style.display = "flex";
}
function offLoading() {
  document.getElementById("loading").style.display = "none";
}
// render ra man hinh
function renderFoodList(array) {
  var contentHTML = "";
  array.forEach(function (product) {
    var contentTr = `<tr>       
                                <td>${product.id}</td>
                                <td>${product.name}</td>
                                <td>${product.type}</td>
                                <td>${product.price}$</td>
                                <td>${product.screen}</td>
                                <td>${product.backCamera}</td>
                                <td>${product.frontCamera}</td>
                                <td class="d-flex align-items-center justify-content-center"> <img src="${product.img}" style="max-width:100%; width:100%" /> </td>
                                <td>${product.desc}</td>
                                <td>
                                    <button class="btn btn-danger mb-3" onclick="deleteProduct(${product.id})"}">
                                    Xoá
                                    </button>
                                    <button class="btn btn-warning" onclick="editProduct(${product.id})"}">
                                    Sửa
                                    </button>
                                </td>
                            </tr>`;
    contentHTML += contentTr;
  });

  document.getElementById("tblDanhSachSP").innerHTML = `${contentHTML}`;
}
function getDataFromForm() {
  var id = document.getElementById("IdSP").value;
  var name = document.getElementById("TenSP").value;
  var type = document.getElementById("loaiSP").value;
  var price = document.getElementById("GiaSP").value;
  var screen = document.getElementById("ManHinhSP").value;
  var backCamera = document.getElementById("CameraSau").value;
  var frontCamera = document.getElementById("CameraTruoc").value;
  var img = document.getElementById("HinhSP").value;
  var desc = document.getElementById("MoTa").value;

  return {
    id: id,
    name: name,
    type: type,
    price: price,
    screen: screen,
    backCamera: backCamera,
    frontCamera: frontCamera,
    img: img,
    desc: desc,
  };
}
function displayDataOnForm(product) {
  document.getElementById("IdSP").value = product.id;
  document.getElementById("TenSP").value = product.name;
  document.getElementById("loaiSP").value = product.type;
  document.getElementById("GiaSP").value = product.price;
  document.getElementById("ManHinhSP").value = product.screen;
  document.getElementById("CameraSau").value = product.backCamera;
  document.getElementById("CameraTruoc").value = product.frontCamera;
  document.getElementById("HinhSP").value = product.img;
  document.getElementById("MoTa").value = product.desc;
}

function addNew() {
  document.getElementById("addSP").disabled = false;
  document.getElementById("IdSP").disabled = false;
  document.getElementById("updateSP").disabled = true;
  document.getElementById("IdSP").value = "";
  document.getElementById("TenSP").value = "";
  document.getElementById("loaiSP").value = "";
  document.getElementById("GiaSP").value = "";
  document.getElementById("ManHinhSP").value = "";
  document.getElementById("CameraSau").value = "";
  document.getElementById("CameraTruoc").value = "";
  document.getElementById("HinhSP").value = "";
  document.getElementById("MoTa").value = "";
}
